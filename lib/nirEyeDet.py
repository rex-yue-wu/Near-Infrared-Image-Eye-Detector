from keras.layers import Conv2D, MaxPool2D, UpSampling2D, Input, Add, Concatenate
from keras.models import Model

def create_grid_model( input_shape=(None,None,1), initial_weights=None )  :
    '''create a Fully Convolutional Network (FCN) for NIR eye detection
    Note:
        this is a grid based model, i.e. it predicts `eye location` in image grids
    '''
    img = Input( shape=input_shape, name='img_in')
    # block 1: 128x128
    nb_filters = 16
    x = Conv2D( nb_filters, (3,3), activation='relu', padding='same', name='b1c1')( img )
    f1 = Conv2D( nb_filters, (1,1), activation='relu', padding='same', name='b1c2')( x )
    # block 2: 64x64
    nb_filters *=2
    x = MaxPool2D((3,3), strides=(2,2), padding='same', name='pool2')(f1)
    x = Conv2D( nb_filters, (3,3), activation='relu', padding='same', name='b2c1')( x )
    f2 = Conv2D( nb_filters, (1,1), activation='relu', padding='same', name='b2c2')( x )
    # block 3: 32x32
    nb_filters *=2
    x = MaxPool2D((3,3), strides=(2,2), padding='same', name='pool3')(f2)
    x = Conv2D( nb_filters, (3,3), activation='relu', padding='same', name='b3c1')( x )
    f3 = Conv2D( nb_filters, (1,1), activation='relu', padding='same', name='b3c2')( x )
    # block 4: 16x16
    nb_filters *=2
    x = MaxPool2D((3,3), strides=(2,2), padding='same', name='pool4')(f3)
    x = Conv2D( nb_filters, (3,3), activation='relu', padding='same', name='b4c1')( x )
    nb_filters *=2
    x = Conv2D( nb_filters, (3,3), activation='relu', padding='same', name='b4c2')( x )
    f5 = Conv2D( nb_filters, (1,1), activation='relu', padding='same', name='b4c3')( x )
    pred = Conv2D(1,(3,3),activation='sigmoid', padding='same', name='pred')( f5 )
    model = Model( inputs=img, outputs=pred, name='EyeGridDet')
    if ( initial_weights is not None ) :
        model.load_weights( initial_weights )
    return model

def create_multi_res_model( input_shape=(None,None,1), initial_weights=None )  :
    '''create a multi resolution FCN model 
    '''
    img = Input( shape=input_shape, name='img_in')
    # block 1: 128x128
    nb_filters = 16
    x = Conv2D( nb_filters, (3,3), activation='relu', padding='same', name='b1c1')( img )
    f1 = Conv2D( nb_filters, (1,1), activation='relu', padding='same', name='b1c2')( x )
    # block 2: 64x64
    nb_filters *=2
    x = MaxPool2D((3,3), strides=(2,2), padding='same', name='pool2')(f1)
    x = Conv2D( nb_filters, (3,3), activation='relu', padding='same', name='b2c1')( x )
    f2 = Conv2D( nb_filters, (1,1), activation='relu', padding='same', name='b2c2')( x )
    # block 3: 32x32
    nb_filters *=2
    x = MaxPool2D((3,3), strides=(2,2), padding='same', name='pool3')(f2)
    x = Conv2D( nb_filters, (3,3), activation='relu', padding='same', name='b3c1')( x )
    f3 = Conv2D( nb_filters, (1,1), activation='relu', padding='same', name='b3c2')( x )
    # block 4: 16x16
    nb_filters *=2
    x = MaxPool2D((3,3), strides=(2,2), padding='same', name='pool4')(f3)
    x = Conv2D( nb_filters, (3,3), activation='relu', padding='same', name='b4c1')( x )
    x = Conv2D( nb_filters, (3,3), activation='relu', padding='same', name='b4c2')( x )
    f4 = Conv2D( nb_filters, (1,1), activation='relu', padding='same', name='b4c3')( x )
    # level 1: 16x8=128
    pred_x8 = Conv2D(1,(3,3),activation='sigmoid', padding='same', name='x8')( f4 )
    # level 2: 32x4=128
    x = UpSampling2D(name='up3')(f4)
    fx3 = Concatenate(axis=-1,name='db3cat')([x,f3])
    fx4 = Conv2D( nb_filters, (3,3), activation='relu', padding='same', name='db3c')( fx3 )
    pred_x4 = Conv2D(1,(3,3),activation='sigmoid', padding='same', name='x4')( fx4 )
    # level 3: x2
    x = UpSampling2D(name='up2')(fx4)
    fx2 = Concatenate(axis=-1,name='db2cat')([x,f2])
    fx2 = Conv2D( nb_filters, (3,3), activation='relu', padding='same', name='db2c')( fx2 )
    pred_x2 = Conv2D(1,(3,3),activation='sigmoid', padding='same', name='x2')( fx2 )
    # level 4: x1
    x = UpSampling2D(name='up1')(fx2)
    fx1 = Concatenate(axis=-1,name='db1cat')([x,f1])
    fx1 = Conv2D( nb_filters, (3,3), activation='relu', padding='same', name='db1c')( fx1 )
    pred_x1 = Conv2D(1,(3,3),activation='sigmoid', padding='same', name='x1')( fx1 )
    base = Model( inputs=img, outputs=[pred_x1,pred_x2,pred_x4,pred_x8], name='EyeMulResDet')
    if ( initial_weights is not None ) :
        base.load_weights( initial_weights )
    # create a multireslution network
    pred_from_x2 = UpSampling2D(size=(2,2), name='restored_x2')( pred_x2 )
    pred_from_x4 = UpSampling2D(size=(4,4), name='restored_x4')( pred_x4 )
    pred_from_x8 = UpSampling2D(size=(8,8), name='restored_x8')( pred_x8 )
    all_pred = Add( name='output')([pred_x1, pred_from_x2, pred_from_x4, pred_from_x8] )
    model = Model( inputs=img, outputs=all_pred, name='EyeMulResDet')
    return model



