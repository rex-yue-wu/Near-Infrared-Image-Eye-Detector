# Keras-Tensorflow Fully Convolutional Neural Network For Near-Infrared Image Eye Detection

Author: Yue Wu
Contact: yue_wu@isi.edu

# 1. Dependency

- `keras`: 2.0.7 or above
- `tensorflow`: 1.1.0 or above


# 2. Pretrained Model Weights (`/model`)


| **Model File** | **#Resolution** | **Mirroring** | **Resizing** | **Rotation** | **#Params** | **Size** |
|:--------------|:--------------:|:--------------:|:--------------:|:--------------:|----:|----------:|
| `eyeGridDetector-a.h5` | Single | Yes | N/A | N/A | 465,905 | 1.9M |
| `eyeGridDetector-b.h5` | Single | Yes | (0.80, 1.25) | N/A |465,905 |1.9M |
| `eyeGridDetector-c.h5` | Single | Yes | (0.67, 1.50) | N/A |465,905 |1.9M |
| `eyeGridDetector-d.h5` | Single | Yes | (0.67, 1.50) | (-30,30) |465,905 |1.9M |
| `eyeMulResDetector.h5` | Multiple (4) | Yes | (0.67, 1.50) | (-30,30) | 843,124 | 3.3M |


# 3. Model Architectures (`/lib`)
- nirEyeDet.py 
- should be Python2 and Python3 compatible

# 4. Testing Images (`/data`)
- 6 NIR testing images
- each dimension of 2168x4096

# 5. Demo.ipynb 
Step by step eye detection examples.

*Alternative Google Colab Notebooks*
- [Python2](https://drive.google.com/file/d/1GvzLZGPvySOz-Qc6Lu4hjbWmax2ilott/view?usp=sharing)
- [Python3](https://drive.google.com/file/d/1lRQR6lFpCFbTomRiSq0bXqLRqQDdg3jk/view?usp=sharing)
